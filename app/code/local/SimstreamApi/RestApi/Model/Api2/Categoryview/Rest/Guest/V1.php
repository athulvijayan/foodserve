<?php

/**
 * Override for Magento's Catalog REST API
 */
class SimstreamApi_RestApi_Model_Api2_Categoryview_Rest_Guest_V1 extends Mage_Api2_Model_Resource {

	/**
	 * Retrieve the category
	 * @return category entity
	 */
    protected function _retrieve() {
    	// Mage::log("categories retrieve called");
      $id = $this->getRequest()->getParam('id');
            $cat = Mage::getModel('catalog/category')->load($id);
            $products = Mage::getModel('catalog/product')
                        ->getCollection()
                        ->addAttributeToSelect('*')
                        ->addCategoryFilter($cat)
                        ->addAttributeToFilter('status', 1) // enabled
                        ->addAttributeToFilter('visibility', 4) //visibility in catalog,search
                        ->addAttributeToSort('created_at', 'desc');
                    if( count($products) > 0 ) {
                      $i=0;
                        foreach($products as $product) {

                            $Products['productId'] = $product->getId();
                            $Products['productName'] = $product->getName();
                            $Products['productImage'] = Mage::getModel('catalog/product_media_config')->getMediaUrl( $product->getImage() );
                            $Products['productPrice'] = $product->getPrice();
                            $Products['productDiscountedprice'] = $product->getFinalPrice(1);
                            $response[$i++] = $Products;
                        }
                        //$response['products'] = $Products;
                    }
                    else {
                        $response['products'] = Mage::helper('customrestapi')->__('Sorry! There are no products with your selection');
                    }

    	   echo json_encode($response);
    }

    /**
     * Retrieves the category collection and returns
     *
     * @return int
     */
  protected function _retrieveCollection() {
    $response=array();
    $i=0;
    $collection = Mage::getModel("categorypromo/promo")->getCollection()->addFieldToFilter('status','0')->addFieldToSelect('*')->setOrder('position','ASC');
    foreach($collection as $department) {
      $data = array();
      $data['departmentId']=$department->getCategory();
      $data['heading'] = $department->getHeading();
      $data['content'] = $department->getContent();
      $data['link'] = $department->getLink();
      $data['image'] = Mage::getBaseUrl('media').$department->getImage();
    $response[$i]=$data;
    $i++;
    }
	  echo json_encode($response);
    //return json_encode($response);
	 }

}

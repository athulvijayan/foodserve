<?php
/* Simstream Login Api
*
* @category SimstreamApi
* @package SimstreamApi_LoginApi
* @author Simstream Development Team
*/
class SimstreamApi_RestApi_Model_Api2_Review_Rest_Guest_V1 extends Mage_Api2_Model_Resource {
    /**
     * Validate user email and password and return userid on successful.
     * Expected parameters are:
     * {
     *    'username': string, - Customer email id
     *    'password': string, - customer password
     * }
     * @return Array
     */

    protected function _create(array $data){

      $review = Mage::getModel('review/review');
      $review->setEntityPkValue($data['productId']);//product id
      $review->setStatusId(0);
      $review->setTitle($data['reviewTitle']);
      $review->setDetail($data['reviewDesc']);
      $review->setEntityId(1);
      $review->setStoreId(0);      //storeview
     $review->setStatusId(2); //approved
      $review->setCustomerId($data['userId']);
      $review->setNickname($data['reviewNick']);
      $review->setReviewId($review->getId());
      $review->setStores(1);
      $review->save();
      $review->aggregate();
      $rating_value=$data['rating'];
      $rating_options = array(
          4 => array(16,17,18,19,20)
        );

        foreach($rating_options as $rating_id => $option_ids):
          try {
            $_rating = Mage::getModel('rating/rating')
              ->setRatingId($rating_id)
              ->setReviewId($review->getId())
              ->addOptionVote($option_ids[$rating_value-1],$data['productId']);
            } catch (Exception $e) {
              die($e->getMessage());
            }
          endforeach;

      $response['status']=1;
        echo json_encode($response);
       return json_encode($response);

}
protected function _retrieve() {
      $response=array();
      $i=0;
      $id = $this->getRequest()->getParam('id');     
      $reviews = Mage::getModel('review/review')->getResourceCollection()
                       ->addStoreFilter(Mage::app()->getStore()->getId())
                       ->addEntityFilter('product', $id)
                       ->addStatusFilter(Mage_Review_Model_Review::STATUS_APPROVED)
                       ->setDateOrder()
                       ->addRateVotes();
          
      $_items = $reviews->getItems();
      $data = array(); 
      $data['rating'] = 0;
      foreach ($_items as $_review){
             $_votes = $_review->getRatingVotes(); 
             foreach ($_votes as $_vote){
                     $data['rating']=($_vote->getPercent()/20);
             }
             $data['reviewId']=$_review->getReviewId();
             $data['customerName']=$_review->getNickname();
             $data['reviewTitle']=$_review->getTitle();
             $data['reviewDesc']=$_review->getDetail();
             $response[$i]=$data;
             $i++;
       }
       echo json_encode($response);
       }
}


 /*    
die;
     }




     echo count($_items); die;
     /*if (count($reviews) > 0)
     { 

        foreach ($reviews->getItems() as $reviewss) {
             foreach( $reviewss->getRatingVotes() as $vote ) {
                 $totalrv = $totalrv +$vote->getValue();
                 $count++;
                 $totalrvper = $totalrvper + $vote->getPercent();
                 }
         }
         $avg= $totalrv/count($reviews); 
         $avgper = round($totalrvper/$count ,1);
       }
    echo $avgper; die;
     */
    /*        foreach ($reviews as $review) {
              $data = array();

              $data['reviewId']=$review->getReviewId();
              $data['customerName']=$review->getNickname();
              $data['reviewTitle']=$review->getTitle();
              $data['reviewDesc']=$review->getDetail();
              
              ///$rating = $review->getItems();
              //print_r($rating);
               //die;                          
              $response[$i]=$data;
              $i++;
            }
echo json_encode($response);
}
}
<?php

/**
 * Override for Magento's Catalog REST API
 */
class SimstreamApi_RestApi_Model_Api2_Category_Rest_Customer_V1 extends Mage_Api2_Model_Resource {

	/**
	 * Retrieve the category
	 * @return category entity
	 */
    protected function _retrieve() {
    	// Mage::log("categories retrieve called");
    	$category_id = $this->getRequest()->getParam('id');
        $category = Mage::getModel('catalog/category')->load($category_id);
        $category->setStoreId($this->_getStore()->getId());

        $data = array();
    	$data['entity_id'] = $category_id;
    	$data['name'] = $category->getName();
    	$data['parent_id'] = $category->getParentId();
    	$data['children'] = $category->getResource()->getChildren($category, false);
    	$data['is_active'] = $category->getIsActive() ? 1 : 0;
    	$data['level'] = $category->getLevel();
    	$data['position'] = $category->getPosition();
    	$data['url_key'] = $category->getUrlKey();
    	return $data;
    }

    /**
     * Retrieves the category collection and returns
     *
     * @return int
     */
     protected function _retrieveCollection() {

	 $level = $this->getRequest()->getParam('level');
	 if($level==''){
	 return $this->getAllCategory();
	 }else{
	 return $this->getCategoryLevel($level);
	 }
	 }



	 function getCategoryLevel($level){
		$storeId = Mage::app()->getStore()->getId();
//	Mage::app()->getLocale()->setLocale("fr_FR");
//	Mage::app()->getLocale()->setDefaultLocale("fr_FR");
	$children = Mage::getModel('catalog/category')->setStoreId($storeId)->getCollection()
	->addAttributeToSelect('*') ->addIsActiveFilter()->addAttributeToFilter('level',$level);
 	$i = 0;
	$cat = array();
    foreach ($children as $category)
    {
    	//print_r ($category);exit;
        $category = Mage::getModel('catalog/category')->setStoreId($storeId)->load($category->getId());
        //echo '<li><a href="' . $category->getUrl() . '">' . $category->getName() . '</a></li>';
        $cat[$i]["id"] = $category->getId();
        $cat[$i]["name"] =  $category->getName();
        $cat[$i]["hasChildren"] = $category->hasChildren();
		$i++;
    }
	return $cat;
    }


	function getAllCategory(){
	$storeId = Mage::app()->getStore()->getId();
//	Mage::app()->getLocale()->setLocale("fr_FR");
//	Mage::app()->getLocale()->setDefaultLocale("fr_FR");
	$children = Mage::getModel('catalog/category')->setStoreId($storeId)->getCategories(2);
 	$i = 0;
	$cat = array();
    foreach ($children as $category)
    {
    	//print_r ($category);exit;
        $category = Mage::getModel('catalog/category')->setStoreId($storeId)->load($category->getId());
        //echo '<li><a href="' . $category->getUrl() . '">' . $category->getName() . '</a></li>';
        $cat[$i]["id"] = $category->getId();
        $cat[$i]["name"] =  $category->getName();
        $cat[$i]["hasChildren"] = $category->hasChildren();
		if($cat[$i]["hasChildren"])
		{
			$subCatIDs = explode(",",$category->getChildren());
			$j=0;
			$subcat = array();
			foreach($subCatIDs as $subCatID)
			{
				$subcategory = Mage::getModel('catalog/category')->load($subCatID);
				$subcat[$j]["id"]  = $subcategory->getId();
				$subcat[$j]["name"]  = $subcategory->getName();
				$subcat[$j]["hasChildren"]  = $subcategory->hasChildren();
				if($subcat[$j]["hasChildren"])
				{
					$subCatIDs1 = explode(",",$subcategory->getChildren());
					$k=0;
					$subcat1 = array();
					foreach($subCatIDs1 as $subCatID1)
					{
						$subcategory1 = Mage::getModel('catalog/category')->load($subCatID1);
						$subcat1[$k]["id"]  = $subcategory1->getId();
						$subcat1[$k]["name"]  = $subcategory1->getName();
						$subcat1[$k]["hasChildren"]  = $subcategory1->hasChildren();
						$k++;
					}
		       		$subcat[$j]["children"] = $subcat1;
				}
				$j++;
			}
       		$cat[$i]["children"] = $subcat;
		}
		$i++;
    }
	return $cat;
	}

}

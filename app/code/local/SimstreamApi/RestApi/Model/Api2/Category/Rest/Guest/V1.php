<?php

/**
 * Override for Magento's Catalog REST API
 */
class SimstreamApi_RestApi_Model_Api2_Category_Rest_Guest_V1 extends Mage_Api2_Model_Resource {

	/**
	 * Retrieve the category
	 * @return category entity
	 */
    /*protected function _retrieve() {
    	// Mage::log("categories retrieve called");
    	$category_id = $this->getRequest()->getParam('id');
        $category = Mage::getModel('catalog/category')->load($category_id);
      //  $category->setStoreId($this->_getStore()->getId());

        $data = array();
    	$data['categoryName'] = $category->getName();
    	$data['parentId'] = $category->getParentId();
    	$data['childrens'] = $category->getResource()->getChildren($category, false);
    	$data['isActive'] = $category->getIsActive() ? 1 : 0;
    	$data['level'] = $category->getLevel();
    	$data['position'] = $category->getPosition();
    //	$data['url'] = $category->getUrl();
      $data['smallImage']=Mage::getBaseUrl('media').''.$category->getThumbnail();
      $data['bigImage']=$category->getImageUrl();
    	 echo json_encode($data);
    }*/

	protected function _retrieve() {
	    	// Mage::log("categories retrieve called");
	    	$id = $this->getRequest()->getParam('id');
		if($id==2){
			$cat= $this->getAllCategory($id);
		}else{

			$cat= $this->loadCategoryId($id);
		}
		
		 echo json_encode($cat);
    }

	/**
     * Retrieves the individual category info and returns
     *
     * @return array;
     */
	private function loadCategoryId($id){
		//$category = Mage::getModel('catalog/category')->load($id);
		$storeId = Mage::app()->getStore()->getId();
		$cat = array();
		$i=0;		
		$children = Mage::getModel('catalog/category')->load($id)->getChildrenCategories();
				
		foreach ($children as $category)
    		{
			if($category->getIsActive()):		 	
				$category = Mage::getModel('catalog/category')->setStoreId($storeId)->load($category->getId());        
				$cat[$i]["catId"] = $category->getId();
				$cat[$i]["catName"] =  $category->getName();				
				$cat[$i]["level"] =  $category->getLevel();
				$cat[$i]["position"] =  $category->getPosition();
				$cat[$i]["smallImage"] =  Mage::getBaseUrl('media').''.$category->getThumbnail();			
				$i++;
			endif;
		}
		return 	$cat;
	}


private function getAllCategory($id=""){
	$storeId = Mage::app()->getStore()->getId();
	if($id==""){
		$children = Mage::getModel('catalog/category')->setStoreId($storeId)->getCategories(2);
	}else{
		$children = Mage::getModel('catalog/category')->setStoreId($storeId)->load($id)->getChildrenCategories();
	}
 	$i = 0;
	$cat = array();
    foreach ($children as $category)
    {
    	
        $category = Mage::getModel('catalog/category')->setStoreId($storeId)->load($category->getId()); 
	if($category->getIsActive()){	       
        $cat[$i]["id"] = $category->getId();
        $cat[$i]["name"] =  $category->getName();	
	$cat[$i]["level"] =  $category->getLevel();
	$cat[$i]["position"] =  $category->getPosition();
	$cat[$i]["smallImage"] =  Mage::getBaseUrl('media').''.$category->getThumbnail();	
        $cat[$i]['indent'] = '1';
        $cat[$i]["hasChildren"] = $category->hasChildren();
		if($cat[$i]["hasChildren"])
		{
			$subCatIDs = explode(",",$category->getChildren());
			$j=0;
			$subcat = array();
			foreach($subCatIDs as $subCatID)
			{
									
					$subcategory = Mage::getModel('catalog/category')->load($subCatID);
					if($subcategory->getIsActive()){					
					$subcat[$j]["catId"]  = $subcategory->getId();
					$subcat[$j]["catName"]  = $subcategory->getName();
					//$subcat[$j]['isActive'] = $category->getIsActive() ? 1 : 0;
				    	$subcat[$j]['level'] = $subcategory->getLevel();
				    	$subcat[$j]['position'] = $subcategory->getPosition();
					$subcat[$j]['smallImage']=Mage::getBaseUrl('media').''.$subcategory->getThumbnail();	      	
					$subcat[$j]['indent'] = '2';
					$subcat[$j]["hasChildren"]  = $subcategory->hasChildren();
					if($subcat[$j]["hasChildren"])
					{
						$subCatIDs1 = explode(",",$subcategory->getChildren());
						$k=0;
						$subcat1 = array();
						foreach($subCatIDs1 as $subCatID1)
						{
														
								$subcategory1 = Mage::getModel('catalog/category')->load($subCatID1);
								if($subcategory1->getIsActive()){								
								$subcat1[$k]["subCatId"]  = $subcategory1->getId();
								$subcat1[$k]["subCatName"]  = $subcategory1->getName();
								$subcat1[$k]['level'] = $subcategory1->getLevel();
								$subcat1[$k]['position'] = $subcategory1->getPosition();
								$subcat1[$k]['smallImage']=Mage::getBaseUrl('media').''.$subcategory1->getThumbnail();	      	
			    					$subcat1[$k]['indent'] = '3';
								$subcat1[$k]["hasChildren"]  = $subcategory1->hasChildren();
								$k++;
							}
						}
			       		$subcat[$j]["children"] = $subcat1;
					}
					$j++;
				}
			}
       		$cat[$i]["children"] = $subcat;
		}
		$i++;
    }
	}

	return $cat;
	}

    /**
     * Retrieves the category collection and returns
     *
     * @return int
     */
     protected function _retrieveCollection() {
		
	 $level = $this->getRequest()->getParam('level');
	
	 if($level==''){
	 return $this->getAllCategory();
	 }else{
	 return $this->getCategoryLevel($level);
	 }
	 }



	 function getCategoryLevel($level){
		$storeId = Mage::app()->getStore()->getId();
//	Mage::app()->getLocale()->setLocale("fr_FR");
//	Mage::app()->getLocale()->setDefaultLocale("fr_FR");
	$children = Mage::getModel('catalog/category')->setStoreId($storeId)->getCollection()
	->addAttributeToSelect('*') ->addIsActiveFilter()->addAttributeToFilter('level',$level);
 	$i = 0;
	$cat = array();
    foreach ($children as $category)
    {
    	//print_r ($category);exit;
        $category = Mage::getModel('catalog/category')->setStoreId($storeId)->load($category->getId());
        //echo '<li><a href="' . $category->getUrl() . '">' . $category->getName() . '</a></li>';
        $cat[$i]["deptId"] = $category->getId();
        $cat[$i]["deptName"] =  $category->getName();
          $cat[$i]['indent'] = '0';
        $cat[$i]["hasChildren"] = $category->hasChildren();
		$i++;
    }
	return $cat;
    }


	/*function getAllCategory(){
	$storeId = Mage::app()->getStore()->getId();
//	Mage::app()->getLocale()->setLocale("fr_FR");
//	Mage::app()->getLocale()->setDefaultLocale("fr_FR");
	$children = Mage::getModel('catalog/category')->setStoreId($storeId)->getCategories(2);
	//$children = Mage::getModel('catalog/category')->setStoreId($storeId)->load(2)->getChildrenCategories();
 	$i = 0;
	$cat = array();
    foreach ($children as $category)
    {
    	//print_r ($category);exit;

        $category = Mage::getModel('catalog/category')->setStoreId($storeId)->load($category->getId());
        //echo '<li><a href="' . $category->getUrl() . '">' . $category->getName() . '</a></li>';
        $cat[$i]["catId"] = $category->getId();
        $cat[$i]["catName"] =  $category->getName();
	$cat[$i]["level"] =  $category->getLevel();
	$cat[$i]["position"] =  $category->getPosition();
	$cat[$i]["smallImage"] =  Mage::getBaseUrl('media').''.$category->getThumbnail();	
        $cat[$i]['indent'] = '0';
        $cat[$i]["hasChildren"] = $category->hasChildren();
		if($cat[$i]["hasChildren"])
		{
			$subCatIDs = explode(",",$category->getChildren());
			$j=0;
			$subcat = array();
			foreach($subCatIDs as $subCatID)
			{
				$subcategory = Mage::getModel('catalog/category')->load($subCatID);
				$subcat[$j]["catId"]  = $subcategory->getId();
				$subcat[$j]["catName"]  = $subcategory->getName();
        $subcat[$j]['indent'] = '1';
				$subcat[$j]["hasChildren"]  = $subcategory->hasChildren();
				if($subcat[$j]["hasChildren"])
				{
					$subCatIDs1 = explode(",",$subcategory->getChildren());
					$k=0;
					$subcat1 = array();
					foreach($subCatIDs1 as $subCatID1)
					{
						$subcategory1 = Mage::getModel('catalog/category')->load($subCatID1);
						$subcat1[$k]["subCatId"]  = $subcategory1->getId();
						$subcat1[$k]["subCatName"]  = $subcategory1->getName();
            $subcat1[$k]['indent'] = '2';
						$subcat1[$k]["hasChildren"]  = $subcategory1->hasChildren();
						$k++;
					}
		       		$subcat[$j]["children"] = $subcat1;
				}
				$j++;
			}
       		$cat[$i]["children"] = $subcat;
		}
		$i++;
    }
	echo json_encode($cat);
	}*/

}

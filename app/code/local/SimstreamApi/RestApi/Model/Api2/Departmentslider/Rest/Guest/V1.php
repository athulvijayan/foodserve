<?php

/**
 * Override for Magento's Catalog REST API
 */
class SimstreamApi_RestApi_Model_Api2_Departmentslider_Rest_Guest_V1 extends Mage_Api2_Model_Resource {

	/**
	 * Retrieve the category
	 * @return category entity
	 */
    protected function _retrieve() {
    	// Mage::log("categories retrieve called");
    	$department_id = $this->getRequest()->getParam('id');
      $collection1 = Mage::getModel("categoryslider/categoryslider")->getCollection()->addFieldToFilter('status','0')->addFieldToFilter('category',$department_id)->addFieldToSelect('*')->setOrder('position','ASC');
      $i=0;
      $response=array();
      foreach($collection1 as $department){
      $data = array();
      $data['caption'] = $department->getCation();
      $data['link'] = $department->getLink();
      $data['image'] = Mage::getBaseUrl('media').$department->getImage();
      $response[$i]=$data;
      $i++;
      }
      //  $category->setStoreId($this->_getStore()->getId());
    	   echo json_encode($response);
    }

    /**
     * Retrieves the category collection and returns
     *
     * @return int
     */
  protected function _retrieveCollection() {
    $response=array();
    $i=0;
    $collection = Mage::getModel("categoryslider/categoryslider")->getCollection()->addFieldToFilter('status','0')->addFieldToSelect('*')->setOrder('position','ASC');
    foreach($collection as $department) {
      $data = array();
    $data['departmentId'] = $department->getCategory();
    $data['caption'] = $department->getCation();
    $data['link'] = $department->getLink();
    $data['image'] = Mage::getBaseUrl('media').$department->getImage();
    $response[$i]=$data;
    $i++;
    }
	  echo json_encode($response);
    //return json_encode($response);
	 }

}

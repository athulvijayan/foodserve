<?php
class SimstreamApi_RestApi_Model_Api2_Signup_Rest_Guest_V1 extends Mage_Api2_Model_Resource {
    /**
     * Validate user email and password and return userid on successful.
     * Expected parameters are:
     * {
     *    'username': string, - Customer email id
     *    'password': string, - customer password
     * }
     * @return Array
     */
    protected function _create(array $data){

      $store = 1;
      $websiteId = Mage::getModel('core/store')->load($store)->getWebsiteId();
      $customer = Mage::getModel("customer/customer");
        $customer->setWebsiteId($websiteId)
		            ->setStoreId($store)
                ->setFirstname($data['firstName'])
            ->setLastname($data['lastName'])
            ->setEmail($data['email'])
            ->setPassword($data['password']);

      try{
        $CustomerId = $customer->save()->getId();
        $customer->setConfirmation(null);
        $customer->save();
        $customer->sendNewAccountEmail();
        $address = Mage::getModel("customer/address");
				$address->setCustomerId($customer->getId())
				        ->setFirstname($data['firstName'])
				        ->setLastname($data['lastName'])
				        ->setCountryId('IN')
                ->setTelephone($data['contactNumber']);
        try{
				    $address->save();
				}
				catch (Exception $e) {
				    Zend_Debug::dump($e->getMessage());
				}
        if($data['isSignUpForNewsLetter']==1){
        $subscriber = Mage::getModel('newsletter/subscriber');
        $subscriber->setStatus(Mage_Newsletter_Model_Subscriber::STATUS_SUBSCRIBED);
        $subscriber->setSubscriberEmail($data['email']);
        $subscriber->setSubscriberConfirmCode($subscriber->RandomSequence());
        $subscriber->setStoreId(1);
        $subscriber->setCustomerId($customer->getId());
            try {
              $subscriber->save();
            }
            catch (Exception $e) {
                throw new Exception($e->getMessage());
            }
        }
        $data1['device_type']=$data['deviceType'];
        $data1['device_token']=$data['deviceToken'];
        $data1['cid']=$CustomerId;
        $data1['email']=$data['email'];
        $data1['issocial']='2';
        $collection = Mage::getModel("devicetoken/devicetoken")->getCollection()->addFieldToFilter('device_token',$data['deviceToken']);
        if($collection->count()==0){
        $model = Mage::getModel('devicetoken/devicetoken')->addData($data1);
        $model->save();
        }
        else{
          foreach($collection as $item){
              $table_id=$item->getId();
            }
        $model = Mage::getModel('devicetoken/devicetoken')->load($table_id)->addData($data1);
        $model->setId($table_id)->save();
        }

        // $user = Mage::getSingleton('customer/session');
        // $user->login($data['email'], $data['psd']);
        // $user->getCustomer();
        $response['status'] = 1;
        $response['message'] = "Success";
        $response['userDetail'] = array('customerId' => $customer->getId(),
        'firstName' => $data['firstName'],
        'lastName' => $data['lastName'],
        'email' => $data['email']);
          }
      catch (Exception $e) {
        $response['statusCode'] = 2;
        $response['message'] = $e->getMessage();
      }
      echo json_encode($response);
       return json_encode($response);

    }

  }

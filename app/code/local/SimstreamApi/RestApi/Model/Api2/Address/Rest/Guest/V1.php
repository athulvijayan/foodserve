<?php
/**
 * Created by midhun.k@simstream.com.
 */

class SimstreamApi_RestApi_Model_Api2_Address_Rest_Guest_V1 extends Mage_Api2_Model_Resource {

	/**
	 * Retrieve the Address
	 * @return address entity
	 */
	protected function _retrieve()
	{

		$id = $this->getRequest()->getParam('id');	

		$customer = Mage::getModel('customer/customer')->load($id); //insert cust ID

        $customerAddress = array();
        $customerBillingAddressId = Mage::getModel('customer/customer')->load($id)->getDefaultBilling();
        $customerShippingAddressId = Mage::getModel('customer/customer')->load($id)->getDefaultShipping();

       	$i=0;
       	$customerAddress = array();
        foreach ($customer->getAddresses() as $address)
            {           	
                $customerAddress[$i]['addressId'] = $address->getId();            
                $customerAddress[$i]['firstName'] = $address->getFirstname();
                $customerAddress[$i]['lastName'] = $address->getLastname();
                $customerAddress[$i]['telephone'] = $address->getTelephone();
                $customerAddress[$i]['state'] = $address->getRegion();
                $customerAddress[$i]['country'] = $address->getCountry_id();
                $customerAddress[$i]['city'] = $address->getCity();
                $customerAddress[$i]['pincode'] = $address->getPostcode();
                $street = $address->getStreet();
																$customerAddress[$i]['line1'] = $street[0];
																$customerAddress[$i]['line2'] = $street[1];
																$customerAddress[$i]['billing'] = $customerBillingAddressId==$address->getId()?1:2;
																$customerAddress[$i]['shipping'] = $customerShippingAddressId==$address->getId()?1:2;
																$i++;
													}																

            echo json_encode($customerAddress);
		
	}		

	/**
     * Retrieves the address collection and returns
     *
     * @return int
     */
	protected function _retrieveCollection() 
	{
		
	}

}
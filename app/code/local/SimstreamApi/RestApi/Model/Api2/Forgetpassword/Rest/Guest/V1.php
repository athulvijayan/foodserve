<?php
/* Simstream Login Api
*
* @category SimstreamApi
* @package SimstreamApi_LoginApi
* @author Simstream Development Team
*/
class SimstreamApi_RestApi_Model_Api2_Forgetpassword_Rest_Guest_V1 extends Mage_Api2_Model_Resource {
    /**
     * Validate user email and password and return userid on successful.
     * Expected parameters are:
     * {
     *    'username': string, - Customer email id
     *    'password': string, - customer password
     * }
     * @return Array
     */

    protected function _create(array $data){
      // $response['status']='1';
      // $response['message']='Mail Successfully sent to your Mail Id';
      $store = 1;
      $websiteId = Mage::getModel('core/store')->load($store)->getWebsiteId();
      $response = array();
		$CustomerEmail = $data['email'];
		$customer = Mage::getModel('customer/customer');
    $customer->setWebsiteId($websiteId);
    $customer->loadByEmail($CustomerEmail);
    if ($customer->getId()) {
        try {
            $newResetPasswordLinkToken =  Mage::helper('customer')->generateResetPasswordLinkToken();
            $customer->changeResetPasswordLinkToken($newResetPasswordLinkToken);
            $customer->sendPasswordResetConfirmationEmail();
            $response['status']='1';
            $response['message'] = 'An email has been sent to your email id to reset your password';
        } catch (Exception $exception) {
            $response['status']='2';
            $response['message'] = 'Sorry! There is an error while sending email to you';
            Mage::log($exception);
        }
    }
    else {
      $response['status']='2';
      $response['message'] = 'Sorry! You dont have account with us!';
    }
      echo json_encode($response);
      return json_encode($response);
    }
}

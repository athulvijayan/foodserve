<?php
/* Simstream Login Api
*
* @category SimstreamApi
* @package SimstreamApi_LoginApi
* @author Simstream Development Team
*/
class SimstreamApi_RestApi_Model_Api2_Addcart_Rest_Guest_V1 extends Mage_Api2_Model_Resource {
    /**
     * Validate user email and password and return userid on successful.
     * Expected parameters are:
     * {
     *    'username': string, - Customer email id
     *    'password': string, - customer password
     * }
     * @return Array
     */

protected function _create(array $data){

$store_id = 1;
$mageRunCode = Mage::app()->getStore()->getCode();
$mageRunType = 'store';
Mage::app()->setCurrentStore(1);
//Mage::run($mageRunCode, $mageRunType);

$customer_id = $data['customerId'];
$product_id = $data['productId'];
$quantity = $data['qty'];

$customer = Mage::getModel('customer/customer')->load($customer_id);
$product = Mage::getModel('catalog/product')->load($product_id);

$quote = Mage::getModel('sales/quote')->loadByCustomer($customer_id); 
$quote->addProduct($product, $quantity);
$quote->setIsActive(1);
$id = $quote->collectTotals()->save()->getId();
 
if(isset($id)){
$result = array('status'=> '1','message'=> 'Product Successfully added to cart');
}else{
$result = array('status'=> '0','message'=> 'error');
}
echo json_encode($result);
  }
}

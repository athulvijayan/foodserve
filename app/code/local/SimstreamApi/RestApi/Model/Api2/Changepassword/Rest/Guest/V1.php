<?php
/* Simstream Login Api
*
* @category SimstreamApi
* @package SimstreamApi_LoginApi
* @author Simstream Development Team
*/
class SimstreamApi_RestApi_Model_Api2_Changepassword_Rest_Guest_V1 extends Mage_Api2_Model_Resource {
    /**
     * Validate user email and password and return userid on successful.
     * Expected parameters are:
     * {
     *    'username': string, - Customer email id
     *    'password': string, - customer password
     * }
     * @return Array
     */

    protected function _create(array $data){
      // $response['status']='1';
      // $response['message']='Mail Successfully sent to your Mail Id';
      $store = 1;
      $websiteId = Mage::getModel('core/store')->load($store)->getWebsiteId();
      $response = array();
		  $CustomerID = $data['userId'];
		  $customer = Mage::getModel('customer/customer');
      $customer->setWebsiteId($websiteId);
      $customer->load($CustomerID);
      $validate = 0;
      $result = '';
      $customerid = $data['userId'];
      $username = $customer->getEmail();
      $oldpassword = $data['oldPassword'];
      $newpassword = $data['newPassword'];
      $storeid =1;
      try {
        $login_customer_result = Mage::getModel('customer/customer')->setWebsiteId($websiteId)->authenticate($username, $oldpassword);
        $validate = 1;
      }
      catch(Exception $ex) {
        $validate = 0;
      }
      if($validate == 1) {
        try {
          $customer = Mage::getModel('customer/customer')->load($customerid);
          $customer->setPassword($newpassword);
          $customer->save();
          $result = 'Your Password has been Changed Successfully';
          $response['status']='1';
        }
        catch(Exception $ex) {
          $result = 'Error : '.$ex->getMessage();
          $response['status']='2';
        }
      }
else {
     $result = 'Incorrect Old Password.';
     $response['status']='2';
}
$response['message'] = $result;

      echo json_encode($response);
      return json_encode($response);
    }
}

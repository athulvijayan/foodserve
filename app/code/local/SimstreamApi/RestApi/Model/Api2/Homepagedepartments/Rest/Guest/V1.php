<?php

/**
 * Override for Magento's Catalog REST API
 */
class SimstreamApi_RestApi_Model_Api2_Homepagedepartments_Rest_Guest_V1 extends Mage_Api2_Model_Resource {

	/**
	 * Retrieve the category
	 * @return category entity
	 */
    protected function _retrieve() {
    	// Mage::log("categories retrieve called");
    	$department_id = $this->getRequest()->getParam('id');
        $department = Mage::getModel('homemenu/homemenu')->load($department_id);
      //  $category->setStoreId($this->_getStore()->getId());

      $response1 = array();
  //  $data['deptId'] = $department->getMid();
  //  $data['categoryId'] = $department->getCategory();
  //  $data['deptName'] = $department->getName();
//    $data['buttonText'] = $department->getButton_text();
//    $data['displayImageUrl'] = Mage::getBaseUrl('media').$department->getImage();
    $_subcat=explode(',',$department->getSub_categories());
    $i=0;
    Mage::log($_subcat);
    foreach ($_subcat as $cat) {
    //  $category_id = $this->getRequest()->getParam('id');
      $category = Mage::getModel('catalog/category')->load($cat);

      $data = array();
      $data['catId']=$cat;
    	$data['catName'] = $category->getName();
    	$data['id'] = $category->getParentId();
    	$data['level'] = $category->getLevel();
    	$data['position'] = $category->getPosition();
      $data['displayImageUrl']=Mage::getBaseUrl('media').'catalog/category/'.$category->getThumbnail();
      $response1[$i++]=$data;
    }
    //$data['featuredCategories']=$_subcat;
    $response['status']=1;
    $response['catList']=$response1;
    $response['message']="Item Fetched Successfully";
	  echo json_encode($response);
    }

    /**
     * Retrieves the category collection and returns
     *
     * @return int
     */
  protected function _retrieveCollection() {
    $response1=array();
    $response=array();
    $i=0;
    $collection = Mage::getModel("homemenu/homemenu")->getCollection()->addFieldToSelect('*')->addFieldToFilter('status','0')->setOrder('position','ASC');
    foreach($collection as $department) {
      $data = array();
    $data['id'] = $department->getMid();
    $data['deptName'] = $department->getName();
    $data['buttonText'] = $department->getButton_text();
    $data['displayImageUrl'] = Mage::getBaseUrl('media').$department->getImage();

    $response1[$i]=$data;
    $i++;
    }
    $response['status']=1;
    $response['departmentList']=$response1;
    $response['message']="List Fetched Successfully";
	  echo json_encode($response);
    //return json_encode($response);
	 }

}

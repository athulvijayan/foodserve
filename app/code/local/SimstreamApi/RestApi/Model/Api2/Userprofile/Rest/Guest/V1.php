<?php
/**
 * Created by midhun.k@simstream.com.
 */

class SimstreamApi_RestApi_Model_Api2_Userprofile_Rest_Guest_V1 extends Mage_Api2_Model_Resource {

    protected function _create(array $data)
    {  	   	
 		$store = 1;
        $websiteId = Mage::getModel('core/store')->load($store)->getWebsiteId();
        $customer = Mage::getModel('customer/customer')->load($data['userId']);       

			$customer->setWebsiteId($websiteId);
		    $customer->setEmail($data['email']); 
		    $customer->setFirstname($data['firstName']); 
		    $customer->setLastname ($data['lastName']); 

    		try {
			   
    			$customer->save();
			}
			catch (Exception $e) {

			    Zend_Debug::dump($e->getMessage());
			}	    
 	}						                	
}
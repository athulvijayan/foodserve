<?php
/* Simstream Login Api
*
* @category SimstreamApi
* @package SimstreamApi_LoginApi
* @author Simstream Development Team
*/
class SimstreamApi_RestApi_Model_Api2_Login_Rest_Admin_V1 extends Mage_Api2_Model_Resource {
    /**
     * Validate user email and password and return userid on successful.
     * Expected parameters are:
     * {
     *    'username': string, - Customer email id
     *    'password': string, - customer password
     * }
     * @return Array
     */
    protected function _create(array $data){

        $email = $data['email'];
        $userPassword = $data['password'];




        if(empty($email) && empty($userPassword)){
            $response['statusCode'] = '300';
            $response['message'] = "Email id or password blank";
            return json_encode($response);
        }else{
            $customer = mage::getModel("customer/customer")
                ->getCollection()
                ->addAttributetoSelect('*')
                ->addAttributetoFilter('email',$email);
            $customerDetail = $customer->load()->toArray();
            if(!empty($customerDetail)){
                foreach($customerDetail as $data){
                    $password = $data['password_hash'];
                    $userId = $data['entity_id'];
                }
                $result = Mage::helper('core')->validateHash($userPassword, $password);
                if(empty($result)){
                    $response['statusCode'] = '300';
                    $response['message'] = "Incorrect Emailid or Password.";
                }else{
                    $response['statusCode'] = '200';
                    $response['message'] = "Login Successful";
                    $response['userId'] = $userId;
                    $response['email'] = $data['email'];
                    $response['name'] = $data['firstname'] . " ". $data['lastname'];
                }
            }else{
                $response['statusCode'] = '300';
                $response['message'] = "Invalid credentials";
            }
        }


}

<?php
/* Simstream Login Api
*
* @category SimstreamApi
* @package SimstreamApi_LoginApi
* @author Simstream Development Team
*/
class SimstreamApi_RestApi_Model_Api2_Login_Rest_Guest_V1 extends Mage_Api2_Model_Resource {
    /**
     * Validate user email and password and return userid on successful.
     * Expected parameters are:
     * {
     *    'username': string, - Customer email id
     *    'password': string, - customer password
     * }
     * @return Array
     */

    protected function _create(array $data){

        $email = $data['email'];
        $data1['device_type']=$data['deviceType'];
        $data1['device_token']=$data['deviceToken'];
        $customer_fname = $data['firstName'];
        $customer_lname = $data['lastName'];
        $response = array();
        $type=strtolower($data['isLoggedinFromSocial']);
              if($type==1)
              {
                  $customer = mage::getModel("customer/customer")
                      ->getCollection()
                      ->addAttributetoSelect('*')
                      ->addAttributetoFilter('email',$email);
                  $customerDetail = $customer->load()->toArray();
                  if(!empty($customerDetail)){
                      foreach($customerDetail as $data){
                          $customer_id = $data['entity_id'];
                      }
                      $response['status']='1';
                      $response['userDetail'] = array('customerId' => $customer_id,
                          'firstName' => $data['firstname'],
                          'lastName' => $data['lastname'],
                          'email' => $email);
                      $response['message']='Login Successful';
                      // $this->_successMessage("Login Successful.", Mage_Api2_Model_Server::HTTP_OK,$response);
                      // $this->getResponse()->appendBody(json_encode($this->getResponse()->getMessages()));
                      // $this->getResponse()->setHttpResponseCode(Mage_Api2_Model_Server::HTTP_MULTI_STATUS);
                      $data1['cid']=$customer_id;
                      $data1['email']=$email;
                      $data1['issocial']=$type;
                      $collection = Mage::getModel("devicetoken/devicetoken")->getCollection()->addFieldToFilter('device_token',$data1['device_token']);
                      if($collection->count()==0){
                      $model = Mage::getModel('devicetoken/devicetoken')->addData($data1);
                      $model->save();
                      }
                      else{
                        foreach($collection as $item){
                            $table_id=$item->getId();
                          }
                      $model = Mage::getModel('devicetoken/devicetoken')->load($table_id)->addData($data1);
                      $model->setId($table_id)->save();
                      }
                      echo json_encode($response);
                      return(json_encode($response));
                      //return $this;
                  }
                  else
                  {
                      $data['firstName']=$customer_fname;
                      $data['lastName']=$customer_lname;
                      $this->_CreateAccount($data);
                  }
              }
              else{
                  $userPassword = $data['password'];
                  if(empty($email) && empty($userPassword)){
                      $response['status'] = '2';
                      $response['message'] = "Email id or password blank";
                      return json_encode($response);
                  }else{
                  $customer = mage::getModel("customer/customer")
                    ->getCollection()
                    ->addAttributetoSelect('*')
                    ->addAttributetoFilter('email',$email);
                  $customerDetail = $customer->load()->toArray();
                  if(!empty($customerDetail)){
                    foreach($customerDetail as $data){
                      $password = $data['password_hash'];
                      $userId = $data['entity_id'];
                  }
                  $result = Mage::helper('core')->validateHash($userPassword, $password);
                  if(empty($result)){
                      $response['status'] = '2';
                      $response['message'] = "Incorrect Emailid or Password.";
                  }else{
                      $response['status'] = '1';
                      $response['userDetail'] = array('customerId' => $userId,
                          'firstName' => $data['firstname'],
                          'lastName' => $data['lastname'],
                          'email' => $data['email']);
                      $response['message'] = "Login Successful";
                }
                }else{
                $response['status'] = '2';
                $response['message'] = "Invalid credentials";
            }
        }
      }
        echo json_encode($response);
       return json_encode($response);

}
function _CreateAccount($data){
        $store = 1;
        $customer = Mage::getModel("customer/customer");
        $customer_email = $data['email'];  // email address
        $customer   ->setWebsiteId(1)
                    ->setStoreId(1)
                    ->setFirstname($data['firstName'])
                    ->setLastname($data['lastName'])
                    ->setEmail($customer_email);
        try{
            $customer->save();
            $customer->setConfirmation(null);
            $customer->save();
            $customer->sendNewAccountEmail();
            $address = Mage::getModel("customer/address");
            $address->setCustomerId($customer->getId())
                    ->setFirstname($data['firstName'])
                    ->setLastname($data['lastName'])
                    ->setCountryId('IN');
            try{
                $address->save();
            }
            catch (Exception $e) {
                Zend_Debug::dump($e->getMessage());
            }
            $data1['cid']=$customer->getId();
            $data1['email']=$customer_email;
            $data1['issocial']=$type;
            $model = Mage::getModel('devicetoken/devicetoken')->addData($data1);
            $model->save();
              $response['status'] = '1';
              $response['userDetail'] = array('customerId' => $customer->getId(),
              'firstName' => $data['firstName'],
              'lastName' => $data['lastName'],
                'email' => $customer_email);
              $response['message'] = "Login Successful";

        }

        catch (Exception $e) {
          $response['status'] = '2';
          $response['message'] = "Login Failed";

        }
        echo json_encode($response);
       return json_encode($response);
    }
}

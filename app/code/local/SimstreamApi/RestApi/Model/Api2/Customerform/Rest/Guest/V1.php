<?php
/**
 * Override for Magento's Customerform REST API
 */
class Simstream_RestApi_Model_Api2_Customerform_Rest_Guest_V1 extends Mage_Api2_Model_Resource {

	 /**
	 * Retrieve the attribute options
	 * @return attribute options
	 */

    protected function _create(array $data){
	 //$data =  $this->getRequest()->getParams();
	 //die(print_r($data));

		$websiteId = Mage::app()->getWebsite()->getId();
		$store = Mage::app()->getStore();

		$customer = Mage::getModel("customer/customer");
			$customer->setFirstname($data['fname'])
					->setLastname($data['lname'])
					->setEmail($data['email'])
					->setPassword($data['pwd']);

		try{
			$CustomerId = $customer->save()->getId();
			$user = Mage::getSingleton('customer/session');
				$user->login($data['email'], $data['pwd']);
				$user->getCustomer();
				$response['statusCode'] = 200;
				$response['message'] = "Success";
				$response["id"] = $user->getCustomer()->getId();
				$response["email"] = $user->getCustomer()->getEmail();
				return json_encode($response);
		}
		catch (Exception $e) {
			$response['statusCode'] = 300;
			$response['message'] = $e->getMessage();
		}

     return json_encode($response);
    }


}

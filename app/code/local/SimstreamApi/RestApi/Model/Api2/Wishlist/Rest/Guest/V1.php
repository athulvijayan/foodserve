<?php
/* Simstream
* @category SimstreamApi
* @package SimstreamApi_LoginApi
* @author Simstream Development Team
*/
class SimstreamApi_RestApi_Model_Api2_Wishlist_Rest_Guest_V1 extends Mage_Api2_Model_Resource {
    /**
     * Validate user email and password and return userid on successful.
     * Expected parameters are:
     * {
     *    'username': string, - Customer email id
     *    'password': string, - customer password
     * }
     * @return Array
     */

     protected function _create(array $data){


 $store_id = 1;
 $mageRunCode = Mage::app()->getStore()->getCode();
 $mageRunType = 'store';

 Mage::app()->setCurrentStore(1);
 //Mage::run($mageRunCode, $mageRunType);

       $customer_id = $data['customerId'];
   $product_id = $data['productId'];
   $customer = Mage::getModel('customer/customer');
   $wishlist = Mage::getModel('wishlist/wishlist');
   $product = Mage::getModel('catalog/product');
   $store = 1;
   $websiteId = Mage::getModel('core/store')->load($store)->getWebsiteId();
 $customer = Mage::getModel("customer/customer");
 $customer->setWebsiteId($websiteId);


 $customer->load($customer_id);
 $wishlist->loadByCustomer($customer_id);
 $wishlist->addNewItem($product->load($product_id));
 $id = $wishlist->save()->getId();
 if(isset($id)){
 $result = array('status'=> '1','message'=> 'Added Successfully');
 }else{
 $result = array('status'=> '0','message'=> 'error');
 }
 echo json_encode($result);
   }

 /**
    * Retrieving Collection
    */
 protected function _retrieve() {

 $id = $this->getRequest()->getParam('id');
 $store = 1;
 $websiteId = Mage::getModel('core/store')->load($store)->getWebsiteId();
 $customer = Mage::getModel('customer/customer');
 $customer->setWebsiteId($websiteId);
 $customer->load($id);

 if($customer->getId())
   {
    $wishlist = Mage::getModel('wishlist/wishlist')->loadByCustomer($customer, true);
    $wishListItemCollection = $wishlist->getItemCollection();
    $response=array();
    $data = array();
    $i=0;
     foreach($wishListItemCollection as $list)
    {

       $obj = Mage::getModel('catalog/product');
       $item = $obj->load($list->getProductId());
       $data['wishlistid']=$list->getWishlistId();
        $data['name']=$item->getName();
        $data['productid']=$list->getId();
        $data['price']=$item->getPrice();
        $data['qty']=$list->getQty();
        $item = Mage::getModel('catalog/product')->setStoreId($item->getStoreId())->load($item->getProductId());
       if ($item->getId()) :
       $data['imgsrc']=Mage::helper('catalog/image')->init($item, 'small_image')->resize(113, 113); endif;
       $response[$i++]=$data;
    }
   }

   echo json_encode($response);

 }


 /**
    * Remove item
    */
   protected function _delete(){
    //   $id = $this->getRequest()->getParam('id');
        $product_id = $this->getRequest()->getParam('pid');
   $customerId = $this->getRequest()->getParam('id');
   $itemCollection = Mage::getModel('wishlist/item')->getCollection()
         ->addCustomerIdFilter($customerId);
     foreach($itemCollection as $item) {
         if($item->getProduct()->getId() == $product_id){
             $item->delete();
             $result['status'] = 1;
         $result['massage']= "Successfully deleted the item from wishlist ";
         echo json_encode($response);
         }
     }
 }


 protected function _update($data)
   {

   $itemId = $this->getRequest()->getParam('id');
   $qty = $this->getRequest()->getParam('qty');

   $item = Mage::getModel('wishlist/item')->load($itemId);

               if (is_null($qty)) {
                   $qty = $item->getQty();
                   if (!$qty) {
                       $qty = 1;
                   }
               } elseif (0 == $qty) {
                   try {
                       $item->delete();
                   } catch (Exception $e) {
                       Mage::logException($e);
                       $result['message']='Can\'t delete item from wishlist';
                   }
               }

               try {
                   $item->setQty($qty)
                       ->save();
                   $updatedItems++;
               } catch (Exception $e) {
                  $result['message']='Wishlist update successfully';
               }
 }

}
